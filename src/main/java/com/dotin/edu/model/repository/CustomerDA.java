package com.dotin.edu.model.repository;

import com.dotin.edu.model.common.JDBCProvider;
import com.dotin.edu.model.entity.Customer;
import com.dotin.edu.model.entity.CustomerType;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

public class CustomerDA implements AutoCloseable {
    private Connection connection;
    private PreparedStatement preparedStatement;
    private static final Logger logger = Logger.getLogger("customerDA");


    public CustomerDA() {
        try {
            connection = JDBCProvider.getConnection();
            connection.setAutoCommit(true);
        } catch (Exception e) {
            logger.info("connection Error." + e.getMessage());
        }
    }

    public int insert(Customer customer) {

        try {
            String customerId = generateUniqueID(customer);
            customer.setCustomerId(customerId);
            java.sql.Date sqlDateBirthDay = new java.sql.Date(customer.getBirthDay().getTime());
            preparedStatement = connection.prepareStatement("insert into customer (customerId,firstName,lastName,fatherName,birthDay,nationalCode,CustomerType) values (?,?,?,?,?,?,?)");
            preparedStatement.setString(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getFatherName());
            preparedStatement.setDate(5, sqlDateBirthDay);
            preparedStatement.setString(6, customer.getNationalCode());
            preparedStatement.setInt(7, customer.getCustomerType());
            int rowEffected = preparedStatement.executeUpdate();
            return rowEffected;

        } catch (Exception e) {
            logger.info("insert Error." + e.getMessage());
            e.getMessage();
            return 0;
        }


    }

    private String generateUniqueID(Customer customer) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mmssMs");
        String time = String.valueOf(simpleDateFormat.format(new java.util.Date()));
        String customerType = customer.getCustomerType().toString();
        String customerId = "0" + customerType + time;
        return customerId;
    }

    public int update(Customer customer) {
        int rowEffected = 0;
        try {
            preparedStatement = connection.prepareStatement("update Customer set firstName=? , lastName=? ,fatherName=? ,birthDay=? ,nationalCode=?  where CustomerId=?");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getFatherName());
            preparedStatement.setDate(4, (Date) customer.getBirthDay());
            preparedStatement.setString(5, customer.getNationalCode());
            preparedStatement.setString(6, customer.getCustomerId());
            rowEffected = preparedStatement.executeUpdate();
            System.out.println("RE"+rowEffected);
        } catch (Exception e) {
            logger.info("update Error." + e.getMessage());
        }
        return rowEffected;

    }

    public int delete(String customerId) {
        int rowEffected = 0;
        try {
            preparedStatement = connection.prepareStatement("delete from Customer where customerId=?");
            preparedStatement.setString(1, customerId);
            rowEffected = preparedStatement.executeUpdate();
        } catch (Exception e) {
            logger.info("delete Error." + e.getMessage());
        }
        return rowEffected;

    }

    public Customer selectOneById(String customerId) {
        Customer customer = new Customer();
        try {
            preparedStatement = connection.prepareStatement("select * from `customer` where `customerId`=?");
            preparedStatement.setString(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Integer customerTypeValue = resultSet.getInt("customerType");
            CustomerType customerType = CustomerType.getCustomerType(customerTypeValue);

            if (customerType.equals(CustomerType.REAL)) {
                customer = new Customer(resultSet.getString("customerId"), resultSet.getString("firstName"), resultSet.getString("lastName"), resultSet.getString("fatherName"), resultSet.getString("nationalCode"), resultSet.getDate("birthDay"), resultSet.getInt("customerType"));
                return customer;
            } else if (customerType.equals(CustomerType.LEGAL)) {
                customer = (new Customer(resultSet.getString("customerId"), resultSet.getString("firstName"), resultSet.getString("nationalCode"), resultSet.getDate("birthDay"), resultSet.getInt("customerType")));
                return customer;

            } else
                return customer;


        } catch (Exception e) {
            logger.info("select Error." + e.getMessage());
            return customer;
        }


    }

    public List<Customer> select(Customer customer ,Date birthDayTo ) {
        String selectQuery = "select * from `customer`";
        List<Customer> customerList = new ArrayList<>();
        int customerTypeIndex=0;
        int birthDayIndex=0;
        int findIndex=1;
        int firstNameIndex=0;
        int lastNameIndex=0;
        int customerIdIndex=0;
        int nationalCodeIndex=0;
        if(customer.getCustomerType() !=null && !customer.getCustomerType().equals("")){
            selectQuery +=" where `customerType`=?";
            customerTypeIndex=findIndex;
            findIndex++;

        }
        if(customer.getBirthDay()!=null && !customer.getBirthDay().equals("")){
            selectQuery +=" and `birthDay`>= ? and `birthDay` <= ?";
            birthDayIndex=findIndex;
            findIndex +=2;

        }
        if(customer.getFirstName() !=null && !customer.getFirstName().equals("")){
            selectQuery +=" and `firstName`=?";
            firstNameIndex=findIndex;
            findIndex++;

        }
        if(customer.getLastName() !=null && !customer.getLastName().equals("")){
            selectQuery +=" and `lastName`=?";
            lastNameIndex=findIndex;
            findIndex++;
        }
        if(customer.getCustomerId() !=null && !customer.getCustomerId().equals("")){
            selectQuery +=" and `customerId`=?";
            customerIdIndex=findIndex;
            findIndex++;
        }
        if(customer.getNationalCode()!=null && !customer.getNationalCode().equals("")){
            selectQuery +=" and `nationalCode`=?";
            nationalCodeIndex=findIndex;
            findIndex++;

        }

        try{
            preparedStatement=connection.prepareStatement(selectQuery);
            System.out.println(selectQuery);
            if(customerTypeIndex>0) {
                System.out.println(customerTypeIndex);
                preparedStatement.setInt(customerTypeIndex, customer.getCustomerType());
            }
            if(birthDayIndex>0){
                System.out.println(customer.getBirthDay());
                System.out.println(birthDayTo);
                preparedStatement.setDate(birthDayIndex, (Date) customer.getBirthDay());
                preparedStatement.setDate(birthDayIndex+1 ,birthDayTo);
            }
            if(firstNameIndex>0) {
                System.out.println(firstNameIndex);
                preparedStatement.setString(firstNameIndex, customer.getFirstName());
            }
            if(lastNameIndex>0)
                preparedStatement.setString(lastNameIndex,customer.getLastName());
            if(customerIdIndex>0)
                preparedStatement.setString(customerIdIndex,customer.getCustomerId());
            if(nationalCodeIndex>0)
                preparedStatement.setString(nationalCodeIndex,customer.getNationalCode());

            ResultSet resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                customerList.add(new Customer(resultSet.getString("customerId"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("fatherName"),resultSet.getString("nationalCode"),resultSet.getDate("birthDay"),resultSet.getInt("customerType")));

            }
            return customerList;


        }catch (Exception e){
            logger.info("select Error:"+e.getMessage());
            return customerList;

        }
        /*
        if (customer.getCustomerType() != null) {
            int indexCustomerType = customer.getCustomerType();
            System.out.println(customer.getCustomerType());
            System.out.println(customer.getFirstName());
            CustomerType customerTypeValue = CustomerType.getCustomerType(indexCustomerType);
            try {
                switch (customerTypeValue) {
                    case REAL:
                        selectQuery += "where `customerId`=? or `firstName`=? or `lastName`=? or `nationalCode`=? ";
                        preparedStatement = connection.prepareStatement(selectQuery);
                        preparedStatement.setString(1, customer.getCustomerId());
                        preparedStatement.setString(2, customer.getFirstName());
                        preparedStatement.setString(3, customer.getLastName());
                        preparedStatement.setString(4, customer.getNationalCode());
                        ResultSet realCustomerResultSet = preparedStatement.executeQuery();
                        System.out.println(realCustomerResultSet.getString("customerId"));
                        while (realCustomerResultSet.next()) {
                            customerList.add(new Customer(realCustomerResultSet.getString("customerId"), realCustomerResultSet.getString("firstName"), realCustomerResultSet.getString("lastName"), realCustomerResultSet.getString("fatherName"), realCustomerResultSet.getString("nationalCode"), realCustomerResultSet.getDate("birthDay"), realCustomerResultSet.getInt("customerType")));
                        }
                        return customerList;

                    case LEGAL:
                        selectQuery += "where firstName=? and nationalCode=? and customerId=?";
                        preparedStatement = connection.prepareStatement(selectQuery);
                        preparedStatement.setString(1, customer.getFirstName());
                        preparedStatement.setString(2, customer.getNationalCode());
                        preparedStatement.setString(3, customer.getCustomerId());
                        ResultSet legalCustomerResultSet = preparedStatement.executeQuery();
                        while (legalCustomerResultSet.next()) {
                            customerList.add(new Customer(legalCustomerResultSet.getString("customerId"), legalCustomerResultSet.getString("firstName"), legalCustomerResultSet.getString("nationalCode"), legalCustomerResultSet.getDate("birthDay"), legalCustomerResultSet.getInt("customerType")));
                        }
                        return customerList;
                    default:
                        return customerList;


                }


            } catch (Exception e) {
                logger.info("select Error." + e.getMessage());
            }


        }
        return customerList;

         */


    }

    public void close() throws Exception {
        connection.commit();
        preparedStatement.close();
        connection.close();
    }
}
