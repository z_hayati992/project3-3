package com.dotin.edu.model.common;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCProvider {
    private static BasicDataSource basicDataSource = new BasicDataSource();

    static {
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/Bank?useUnicode=true&characterEncoding=UTF-8");
        basicDataSource.setUsername("admin");
        basicDataSource.setPassword("root1234");
        basicDataSource.setMaxTotal(10);
    }

    public static Connection getConnection() throws SQLException {
        return basicDataSource.getConnection();
    }
}
