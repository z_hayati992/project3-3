package com.dotin.edu.model.entity;

import java.util.Date;

public class Customer {
    private String customerId;
    private String firstName;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private Date birthDay;
    private Integer customerType;

    public Customer() {
    }


    public Customer(String customerId, String firstName, String lastName, String fatherName, String nationalCode, Date birthDay, Integer customerType) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.nationalCode = nationalCode;
        this.birthDay = birthDay;
        this.customerType = customerType;
    }

    public Customer(String customerId, String firstName, String nationalCode, Date birthDay, Integer customerType) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.nationalCode = nationalCode;
        this.birthDay = birthDay;
        this.customerType = customerType;
    }

    public String getCustomerId() {

        return customerId;
    }

    public void setCustomerId(String customerId) {

        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {

        this.fatherName = fatherName;
    }

    public String getNationalCode() {

        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Date getBirthDay() {

        return birthDay;
    }

    public void setBirthDay(Date birthDay) {

        this.birthDay = birthDay;
    }

    public Integer getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Integer customerType) {
        this.customerType = customerType;
    }


}
