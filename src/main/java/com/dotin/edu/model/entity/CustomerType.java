package com.dotin.edu.model.entity;

public enum CustomerType {
    REAL(0),LEGAL(1);
    private int index;
    CustomerType(int index){
        this.index=index;

    }
    public int getIndex(){
        return index;
    }
    public static CustomerType getCustomerType(int index){
        for (CustomerType customerTypeValue: CustomerType.values() ) {
            if(customerTypeValue.getIndex()==index)
                return customerTypeValue;

        }
        return null;

    }

}
