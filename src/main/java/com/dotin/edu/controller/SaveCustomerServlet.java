package com.dotin.edu.controller;

import com.dotin.edu.model.common.Converter;
import com.dotin.edu.model.entity.Customer;
import com.dotin.edu.model.entity.CustomerType;
import com.dotin.edu.model.repository.CustomerDA;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.List;

@WebServlet(name ="SaveCustomerServlet", urlPatterns ="/register")
public class SaveCustomerServlet extends HttpServlet {
    public void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException, ServletException {
        CustomerType customerType;
        servletRequest.setCharacterEncoding("UTF-8");
        String firstName = servletRequest.getParameter("firstName");
        String lastName = servletRequest.getParameter("lastName");
        String fatherName =servletRequest.getParameter("fatherName");
        String birthDay = servletRequest.getParameter("birthDay");
        String birthMonth = servletRequest.getParameter("birthMonth");
        String birthYear = servletRequest.getParameter("birthYear");
        String nationalCode = servletRequest.getParameter("nationalCode");
        String customerTypeValue = servletRequest.getParameter("customerType");
        if (customerTypeValue == null) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            customerType = CustomerType.getCustomerType(Integer.parseInt(customerTypeValue));
        } catch (Exception e) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        if (customerType.equals(CustomerType.REAL)) {
            if (firstName == null || firstName.equals("") || lastName == null || lastName.equals("") || fatherName == null || fatherName.equals("") || birthDay == null || birthDay.equals("") ||
                    birthMonth == null || birthMonth.equals("") || birthYear == null || birthYear.equals("") || nationalCode == null || nationalCode.equals("")) {
                servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else if (firstName.length() > 30 || lastName.length() > 60 || fatherName.length() > 30 || birthDay.length() > 2 || birthMonth.length() > 2 ||
                    birthYear.length() > 4 || nationalCode.length() > 10) {
                servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                int birthDayInt=Integer.parseInt(birthDay);
                int birthMonthInt=Integer.parseInt(birthMonth);
                int birthYearInt=Integer.parseInt(birthYear);
                if(birthDayInt<1 ||birthDayInt>31 || birthMonthInt<1 ||birthMonthInt>12 || birthYearInt<1000 ||birthYearInt>1400){
                    try {
                        throw new Exception("Date Invalid.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                int[] birthDayConvert= Converter.jalali_to_gregorian(birthYearInt,birthMonthInt,birthDayInt);
                Date birthDayDate=Date.valueOf(birthDayConvert[0]+"-"+birthDayConvert[1]+"-"+birthDayConvert[2]);
                Customer customer = new Customer();
                customer.setFirstName(firstName);
                customer.setLastName(lastName);
                customer.setFatherName(fatherName);
                customer.setNationalCode(nationalCode);
                customer.setCustomerType(customerType.getIndex());
                customer.setBirthDay(birthDayDate);
                CustomerDA customerDA = new CustomerDA();
                Customer customerTemp=new Customer();
                customerTemp.setNationalCode(nationalCode);
                customerTemp.setCustomerType(Integer.parseInt(customerTypeValue));
                List<Customer> customerList=customerDA.select(customerTemp,birthDayDate);
                if (!(customerList.isEmpty())) {
                    customer.setCustomerId(customerList.get(0).getCustomerId());
                    servletRequest.setAttribute("real", customer);
                    servletRequest.setAttribute("found", true);
                    servletRequest.getRequestDispatcher("/view/customer/real/showAddResult.jsp").forward(servletRequest, servletResponse);
                    return;

                }
                servletRequest.setAttribute("found", false);
                int rowEffected = customerDA.insert(customer);
                System.out.println(rowEffected);
                servletRequest.setAttribute("customer", customer);
                servletRequest.getRequestDispatcher("/view/customer/real/showAddResult.jsp").forward(servletRequest, servletResponse);


            }
        }
        else if(customerType.equals(CustomerType.LEGAL)) {
            if (firstName == null || firstName.equals("") || birthDay == null || birthDay.equals("") || birthMonth == null ||
                    birthMonth.equals("") || birthYear == null || birthYear.equals("") || nationalCode == null || nationalCode.equals("")) {
                servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else if (firstName.length() > 30 || birthDay.length() > 2 || birthMonth.length() > 2 || birthYear.length() > 4 || nationalCode.length() > 10) {
                servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

            } else {
                int birthDayInt=Integer.parseInt(birthDay);
                int birthMonthInt=Integer.parseInt(birthMonth);
                int birthYearInt=Integer.parseInt(birthYear);
                if(birthDayInt<1 ||birthDayInt>31 || birthMonthInt<1 ||birthMonthInt>12 || birthYearInt<1000 ||birthYearInt>1400){
                    try {
                        throw new Exception("Date Invalid.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                int[] birthDayConvert= Converter.jalali_to_gregorian(birthYearInt,birthMonthInt,birthDayInt);
                Date birthDayDate=Date.valueOf(birthDayConvert[0]+"-"+birthDayConvert[1]+"-"+birthDayConvert[2]);
                Customer customer = new Customer();
                customer.setFirstName(firstName);
                customer.setNationalCode(nationalCode);
                customer.setCustomerType(customerType.getIndex());
                customer.setBirthDay(birthDayDate);
                CustomerDA customerDA = new CustomerDA();
                Customer customerTemp=new Customer();
                customerTemp.setNationalCode(nationalCode);
                customerTemp.setCustomerType(Integer.parseInt(customerTypeValue));
                 List<Customer> customerList=customerDA.select(customerTemp ,birthDayDate);
                if (!(customerList.isEmpty())){
                    customer.setCustomerId(customerList.get(0).getCustomerId());
                    servletRequest.setAttribute("legal", customer);
                    servletRequest.setAttribute("found", true);
                    servletRequest.getRequestDispatcher("/view/customer/legal/showAddResult.jsp").forward(servletRequest, servletResponse);
                    return;

                }
                else {
                    servletRequest.setAttribute("found", false);
                    int rowEffected = customerDA.insert(customer);
                    servletRequest.setAttribute("customer", customer);
                    servletRequest.getRequestDispatcher("/view/customer/legal/showAddResult.jsp").forward(servletRequest, servletResponse);
                }

            }
        }


    }

}
