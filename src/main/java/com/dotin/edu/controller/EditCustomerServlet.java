package com.dotin.edu.controller;

import com.dotin.edu.model.common.Converter;
import com.dotin.edu.model.entity.Customer;
import com.dotin.edu.model.entity.CustomerType;
import com.dotin.edu.model.repository.CustomerDA;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@WebServlet(name = "EditCustomerServlet", urlPatterns = "/customer/EditCustomer")
public class EditCustomerServlet extends HttpServlet{
    public void doPost(HttpServletRequest servletRequest , HttpServletResponse servletResponse) throws ServletException, IOException {
        servletRequest.setCharacterEncoding("UTF-8");
        String firstName=servletRequest.getParameter("firstName");
        String lastName=servletRequest.getParameter("lastName");
        String fatherName=servletRequest.getParameter("fatherName");
        String birthDay=servletRequest.getParameter("birthDay");
        String birthMonth=servletRequest.getParameter("birthMonth");
        String birthYear=servletRequest.getParameter("birthYear");
        String nationalCode=servletRequest.getParameter("nationalCode");
        String customerId=servletRequest.getParameter("customerId");
        String customerTypeIndex=servletRequest.getParameter("customerType");
        CustomerType customerType;

        if (customerTypeIndex == null) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            customerType = CustomerType.getCustomerType(Integer.parseInt(customerTypeIndex));
        } catch (Exception e) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        switch (customerType) {

            case REAL: {
                if (firstName == null || firstName.equals("") || lastName == null || lastName.equals("") || fatherName == null || fatherName.equals("") || birthDay == null || birthDay.equals("") ||
                        birthMonth == null || birthMonth.equals("") || birthYear == null || birthYear.equals("") || nationalCode == null || nationalCode.equals("")) {
                    servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                } else if (firstName.length() > 30 || lastName.length() > 60 || fatherName.length() > 30 || birthDay.length() > 2 || birthMonth.length() > 2 ||
                        birthYear.length() > 4 || nationalCode.length() > 10) {
                    servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

                } else {
                    Customer customer = new Customer();
                    int birthDayInt = Integer.parseInt(birthDay);
                    int birthMonthInt = Integer.parseInt(birthMonth);
                    int birthYearInt = Integer.parseInt(birthYear);
                    if (birthDayInt < 1 || birthDayInt > 31 || birthMonthInt < 1 || birthMonthInt > 12 || birthYearInt < 1000 || birthYearInt > 1400) {
                            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                            break;
                    } else {
                        int[] birthDayConvert = Converter.jalali_to_gregorian(birthYearInt, birthMonthInt, birthDayInt);
                        Date birthDayDate = Date.valueOf(birthDayConvert[0] + "-" + birthDayConvert[1] + "-" + birthDayConvert[2]);
                        customer.setFirstName(firstName);
                        customer.setLastName(lastName);
                        customer.setFatherName(fatherName);
                        customer.setNationalCode(nationalCode);
                        customer.setCustomerId(customerId);
                        customer.setBirthDay(birthDayDate);
                        CustomerDA customerDA = new CustomerDA();
                        int rowEffected = customerDA.update(customer);
                        if (rowEffected != 0) {
                            servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/real/showEditResult.jsp?edited=1");
                        } else {
                            servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/real/showEditResult.jsp?edited=0");
                        }
                        break;
                    }
                }
            }
            case LEGAL: {
                if (firstName == null || firstName.equals("") || birthDay == null || birthDay.equals("") || birthMonth == null ||
                        birthMonth.equals("") || birthYear == null || birthYear.equals("") || nationalCode == null || nationalCode.equals("")) {
                    servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                } else if (firstName.length() > 30 || birthDay.length() > 2 || birthMonth.length() > 2 || birthYear.length() > 4 || nationalCode.length() > 10) {
                    servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

                } else {
                    Customer customer = new Customer();
                    int birthDayInt = Integer.parseInt(birthDay);
                    int birthMonthInt = Integer.parseInt(birthMonth);
                    int birthYearInt = Integer.parseInt(birthYear);
                    if (birthDayInt < 1 || birthDayInt > 31 || birthMonthInt < 1 || birthMonthInt > 12 || birthYearInt < 1000 || birthYearInt > 1400) {
                            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                            break;
                    } else {
                        int[] birthDayConvert = Converter.jalali_to_gregorian(birthYearInt, birthMonthInt, birthDayInt);
                        Date birthDayDate = Date.valueOf(birthDayConvert[0] + "-" + birthDayConvert[1] + "-" + birthDayConvert[2]);
                        customer.setFirstName(firstName);
                        customer.setNationalCode(nationalCode);
                        customer.setBirthDay(birthDayDate);
                        customer.setCustomerId(customerId);
                        CustomerDA customerDA = new CustomerDA();
                        int rowEffected = customerDA.update(customer);
                        Customer customerTemp = new Customer();

                        if (rowEffected != 0) {
                            servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/legal/showEditResult.jsp?edited=1");
                        } else {
                            servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/legal/showEditResult.jsp?edited=0");
                        }

                    }
                }
            }

        }
    }



    public void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        String forward = "";
        String action = servletRequest.getParameter("action");
        if (action.equalsIgnoreCase("delete")) {
            CustomerDA customerDA = new CustomerDA();
            Integer rowEffected=customerDA.delete(servletRequest.getParameter("customerId"));
            String customerTypeValue=servletRequest.getParameter("customerType");
           CustomerType customerType = CustomerType.getCustomerType(Integer.parseInt(customerTypeValue));
            servletRequest.setAttribute("deleted",rowEffected);
            if(customerType.equals(CustomerType.REAL)) {
                if (rowEffected != 0) {
                    servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/real/showDeleteResult.jsp?deleted=1");
                } else {
                    servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/real/showDeleteResult.jsp?deleted=0");
                }
            } else {
                if (rowEffected != 0) {
                    servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/legal/showDeleteResult.jsp?deleted=1");
                } else {
                    servletResponse.sendRedirect(getServletContext().getContextPath() + "/view/customer/legal/showDeleteResult.jsp?deleted=0");
                }
            }

        } else if (action.equalsIgnoreCase("edit")) {
            String customerTypeValue=servletRequest.getParameter("customerType");
            String customerId=servletRequest.getParameter("customerId");
            CustomerType customerType=CustomerType.getCustomerType(Integer.parseInt(customerTypeValue));
            if(customerType.equals(CustomerType.REAL)) {

                CustomerDA customerDA = new CustomerDA();
                forward = "/view/customer/real/editRealCustomer.jsp";
                Customer customer = customerDA.selectOneById(customerId);
                servletRequest.setAttribute("customer", customer);
                RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher(forward);
                requestDispatcher.forward(servletRequest, servletResponse);
            }
            else{
                CustomerDA customerDA = new CustomerDA();
                forward = "/view/customer/legal/editLegalCustomer.jsp";
                Customer customer = customerDA.selectOneById(customerId);
                servletRequest.setAttribute("customer", customer);
                RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher(forward);
                requestDispatcher.forward(servletRequest, servletResponse);

            }
        }

    }
}


