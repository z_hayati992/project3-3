package com.dotin.edu.controller;

import com.dotin.edu.model.common.Converter;
import com.dotin.edu.model.entity.Customer;
import com.dotin.edu.model.entity.CustomerType;
import com.dotin.edu.model.repository.CustomerDA;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "SearchCustomerServlet", urlPatterns = "/customer/SearchCustomer")
public class SearchCustomerServlet extends HttpServlet {
    public void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException, ServletException {
        CustomerType customerType;
        Customer customer = new Customer();
        Date birthDayToDate = null;
        Date birthDayFromDate=null;
        servletRequest.setCharacterEncoding("UTF-8");
        String firstName = servletRequest.getParameter("firstName");
        String lastName = servletRequest.getParameter("lastName");
        String nationalCode = servletRequest.getParameter("nationalCode");
        String customerTypeValue = servletRequest.getParameter("customerType");
        String customerId = servletRequest.getParameter("customerId");
        String birthDayFrom=servletRequest.getParameter("birthDayFrom");
        String birthMonthFrom=servletRequest.getParameter("birthMonthFrom");
        String birthYearFrom=servletRequest.getParameter("birthYearFrom");
        String birthDayTo=servletRequest.getParameter("birthDayTo");
        String birthMonthTo=servletRequest.getParameter("birthMonthTo");
        String birthYearTo=servletRequest.getParameter("birthYearTo");
        if (customerTypeValue == null) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        try {
            customerType = CustomerType.getCustomerType(Integer.parseInt(customerTypeValue));
        } catch (Exception e) {
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        if(birthDayFrom!=null && !(birthDayFrom.trim().equals("")) && birthMonthFrom!=null && !(birthMonthFrom.trim().equals("")) && birthYearFrom!=null && !(birthYearFrom.trim().equals(""))
        || birthDayTo!=null && !(birthDayTo.trim().equals("")) && birthMonthTo!=null && !(birthMonthTo.trim().equals("")) && birthYearTo!=null && !(birthYearTo.trim().equals(""))) {
            if (birthDayFrom.length() > 2 || birthDayTo.length() > 2 || birthMonthFrom.length() > 2 || birthMonthTo.length() > 2 ||
                    birthYearFrom.length() > 4 || birthYearTo.length() > 4) {
                servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } else {
                int birthDayFromInt = Integer.parseInt(birthDayFrom);
                int birthMonthFromInt = Integer.parseInt(birthMonthFrom);
                int birthYearFromInt = Integer.parseInt(birthYearFrom);
                int birthDayToInt = Integer.parseInt(birthDayTo);
                int birthMonthToInt = Integer.parseInt(birthMonthTo);
                int birthYearToInt = Integer.parseInt(birthYearTo);
                if (birthDayFromInt < 1 || birthDayFromInt > 31 || birthDayToInt < 1 || birthDayToInt > 31 || birthMonthFromInt < 1 || birthMonthFromInt > 12 || birthMonthToInt < 1 || birthMonthToInt > 12 || birthYearFromInt < 1000 || birthYearFromInt > 1400
                        || birthYearToInt < 1000 || birthYearToInt > 1400) {
                    servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                } else {
                    int[] birthDayFromConvert = Converter.jalali_to_gregorian(birthYearFromInt, birthMonthFromInt, birthDayFromInt);
                    birthDayFromDate = Date.valueOf(birthDayFromConvert[0] + "-" + birthDayFromConvert[1] + "-" + birthDayFromConvert[2]);
                    int[] birthDayToConvert = Converter.jalali_to_gregorian(birthYearToInt, birthMonthToInt, birthDayToInt);
                    birthDayToDate = Date.valueOf(birthDayToConvert[0] + "-" + birthDayToConvert[1] + "-" + birthDayToConvert[2]);
                    System.out.println(birthDayFromDate);
                    System.out.println(birthDayToDate);
                    if (birthDayFromDate.before(birthDayToDate) || birthDayFromDate.equals(birthDayToDate)) {
                        if (customerType.equals(CustomerType.REAL)) {
                            customer.setBirthDay(birthDayFromDate);
                            customer.setCustomerId(customerId);
                            customer.setFirstName(firstName);
                            customer.setLastName(lastName);
                            customer.setNationalCode(nationalCode);
                            customer.setCustomerType(Integer.parseInt(customerTypeValue));
                            CustomerDA customerDA = new CustomerDA();
                            List<Customer> customerList = customerDA.select(customer, birthDayToDate);
                            if (customerList.isEmpty()) {
                                servletRequest.setAttribute("found", false);
                                servletRequest.getRequestDispatcher("/view/customer/real/showSearchResult.jsp").forward(servletRequest, servletResponse);
                                return;
                            }
                            servletRequest.setAttribute("found", true);
                            servletRequest.setAttribute("customerList", customerList);
                            servletRequest.getRequestDispatcher("/view/customer/real/showSearchResult.jsp").forward(servletRequest, servletResponse);
                        } else if (customerType.equals(CustomerType.LEGAL)) {
                            customer.setBirthDay(birthDayFromDate);
                            customer.setCustomerId(customerId);
                            customer.setFirstName(firstName);
                            customer.setNationalCode(nationalCode);
                            customer.setCustomerType(Integer.parseInt(customerTypeValue));
                            CustomerDA customerDA = new CustomerDA();
                            List<Customer> customerList = customerDA.select(customer, birthDayToDate);
                            if (customerList.isEmpty()) {
                                servletRequest.setAttribute("found", false);
                                servletRequest.getRequestDispatcher("/view/customer/legal/showSearchResult.jsp").forward(servletRequest, servletResponse);
                                return;
                            }
                            servletRequest.setAttribute("found", true);
                            servletRequest.setAttribute("customerList", customerList);
                            servletRequest.getRequestDispatcher("/view/customer/legal/showSearchResult.jsp").forward(servletRequest, servletResponse);


                        }


                    } else
                        servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }

            }
       }
        else if(birthDayFrom.equals("") && birthMonthFrom.equals("") && birthYearFrom.equals("") && birthDayTo.equals("")
        && birthMonthTo.equals("") && birthYearTo.equals("")){
            if (customerType.equals(CustomerType.REAL)) {
                customer.setCustomerId(customerId);
                customer.setFirstName(firstName);
                customer.setLastName(lastName);
                customer.setNationalCode(nationalCode);
                customer.setCustomerType(Integer.parseInt(customerTypeValue));
                CustomerDA customerDA = new CustomerDA();
                List<Customer> customerList = customerDA.select(customer,null);
                if (customerList.isEmpty()) {
                    servletRequest.setAttribute("found", false);
                    servletRequest.getRequestDispatcher("/view/customer/real/showSearchResult.jsp").forward(servletRequest, servletResponse);
                    return;
                }
                servletRequest.setAttribute("found", true);
                servletRequest.setAttribute("customerList", customerList);
                servletRequest.getRequestDispatcher("/view/customer/real/showSearchResult.jsp").forward(servletRequest, servletResponse);
            } else if (customerType.equals(CustomerType.LEGAL)) {
                customer.setCustomerId(customerId);
                customer.setFirstName(firstName);
                customer.setNationalCode(nationalCode);
                customer.setCustomerType(Integer.parseInt(customerTypeValue));
                CustomerDA customerDA = new CustomerDA();
                List<Customer> customerList = customerDA.select(customer ,null);
                if (customerList.isEmpty()) {
                    servletRequest.setAttribute("found", false);
                    servletRequest.getRequestDispatcher("/view/customer/legal/showSearchResult.jsp").forward(servletRequest, servletResponse);
                    return;
                }
                servletRequest.setAttribute("found", true);
                servletRequest.setAttribute("customerList", customerList);
                servletRequest.getRequestDispatcher("/view/customer/legal/showSearchResult.jsp").forward(servletRequest, servletResponse);



            }


        }
        else
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

    }
}
