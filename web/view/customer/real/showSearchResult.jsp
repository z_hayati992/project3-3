<%@ page import="java.util.List" %>
<%@ page import="com.dotin.edu.model.entity.Customer" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.dotin.edu.model.common.Converter" %>
<%@ page import="java.sql.Date" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/13/2020
  Time: 2:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ResourceBundle resourceBundle = ResourceBundle.getBundle("Customer",new Locale("fa"));
%>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
</head>
<body>
<jsp:include page="/menubar.jsp"/>
<%
       if(request.getAttribute("found")==null){
             response.sendError(HttpServletResponse.SC_BAD_REQUEST);
             return;
       }
       if(request.getAttribute("found").equals(false)){%>
           <p>
               <%=resourceBundle.getString("sorry_There_is_not_a_customer_with_these_characteristics")%></p>
              <p><a href="${pageContext.request.contextPath}/view/customer/real/search.jsp"><%=resourceBundle.getString("go_to_back")%></a>
           </p>

       <%}else{%>
       <div class="form" style="text-align:center">
           <p>
           <%=resourceBundle.getString("list_of_customer")%></p>
       </div>
<br>
<div align="center">
    <table border="1" cellpadding="5">
        <tr>
            <th><%=resourceBundle.getString("customerId")%></th>
            <th><%=resourceBundle.getString("firstName")%></th>
            <th><%=resourceBundle.getString("lastName")%></th>
            <th><%=resourceBundle.getString("fatherName")%></th>
            <th><%=resourceBundle.getString("nationalCode")%></th>
            <th><%=resourceBundle.getString("birthDay")%></th>
            <th><%=resourceBundle.getString("actions")%></th>
        </tr>

            <%
                List<Customer> customerList= (List<Customer>) request.getAttribute("customerList");
                for (Customer customer:customerList) {

            %>
        <tr>
                    <td>
                      <%= customer.getCustomerId()%>
                    </td>
        <td>
            <%= customer.getFirstName()%>
        </td>
        <td>
            <%= customer.getLastName()%>
        </td>
        <td>
            <%= customer.getFatherName()%>
        </td>
        <td>
            <%= customer.getNationalCode()%>
        </td>
        <td>
            <%= Converter.jalaliDate((Date) customer.getBirthDay())%>
        </td>
        <td>
        <a href="${pageContext.request.contextPath}/customer/EditCustomer?action=edit&customerId=<%=customer.getCustomerId()%>&customerType=<%=customer.getCustomerType()%>"><%=resourceBundle.getString("edit")%></a>
        <a href="${pageContext.request.contextPath}/customer/EditCustomer?action=delete&customerId=<%=customer.getCustomerId()%>&customerType=<%=customer.getCustomerType()%>"><%=resourceBundle.getString("delete")%></a>
        </td>
        </tr>
        <%}%>

    </table>
</div>


    <% }%>
</body>
</html>
