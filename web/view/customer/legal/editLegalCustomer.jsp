<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.dotin.edu.model.common.Converter" %>
<%@ page import="java.sql.Date" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/13/2020
  Time: 10:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%ResourceBundle resourceBundle = ResourceBundle.getBundle("Customer",new Locale("fa"));%>
<html>
<head>
    <title><%=resourceBundle.getString("edit")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/ValidateLegalCustomer.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/customer-properties.js"></script>
</head>
<body>
<jsp:include page="/menubar.jsp"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/ValidateRealCustomerForm.js"></script>
<div align="center">
    <jsp:useBean id="customer" class="com.dotin.edu.model.entity.Customer" scope="request"/>
    <div>
         <form class="form" name="myForm" method="post" action="${pageContext.request.contextPath}/customer/EditCustomer"
          onsubmit="return(validate(this));">
             <table>
                 <tr>
                     <td>
                         <input id="customerId" maxlength="30" value="${customer.customerId}" type="text" name="customerId" readonly="readonly"/>
                     </td>
                     <td>
                         <label for="customerId"><%=resourceBundle.getString("customerId")%></label>
                     </td>

                 </tr>
                 <tr>
                     <td>
                         <input id="firstName" maxlength="30" value="${customer.firstName}" type="text" name="firstName"/>
                     </td>
                     <td>
                         <label for="firstName"><%=resourceBundle.getString("companyName")%></label>
                     </td>

                 </tr>

                 <tr>
                     <td><%int[] dateItem = Converter.jalaliDateItems((Date) customer.getBirthDay()); %>
                         <input id="birthYear" value="<%=dateItem[0]%>" name="birthYear" maxlength="4" size="3"
                                placeholder="<%=resourceBundle.getString("year")%>" onkeypress="return isNumber()"/>

                         <input id="birthMonth" value="<%=dateItem[1]%>" name="birthMonth" maxlength="2" size="1"
                                placeholder="<%=resourceBundle.getString("month")%>" onkeypress="return isNumber()" />
                         <input id="birthDay" value="<%=dateItem[2]%>" name="birthDay" maxlength="2" size="1"
                                placeholder="<%=resourceBundle.getString("day")%>" onkeypress="return isNumber()"/>

                     </td>
                     <td>
                         <label for="birthDay"><%=resourceBundle.getString("registrationDate")%>:</label>
                     </td>

                 </tr>
                 <tr>
                     <td>
                         <input id="nationalCode" maxlength="10" value="${customer.nationalCode}" type="text"
                                name="nationalCode"  onkeypress="return(isNumber());"/>
                     </td>
                     <td>
                         <label for="nationalCode"><%=resourceBundle.getString("economicCode")%></label>
                     </td>

                 </tr>
                 <tr>
                     <td>
                         <input type="hidden" name="customerType" value="${customer.customerType}"/>
                         <input class="button2"  type="submit" style="width: 100px" value=<%=resourceBundle.getString("save")%>>
                     </td>

                 </tr>
             </table>
         </form>
    </div>
</div>


</body>
</html>
