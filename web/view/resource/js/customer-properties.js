var message= {
    firstName_cannot_be_empty:"لطفا نام را وارد کنید" ,
    lastName_cannot_be_empty:"لطفا نام خانوادگی را وارد کنید" ,
    fatherName_cannot_be_empty:"لطفا نام پدر را وارد کنید" ,
    birthDay_cannot_be_empty:"لطفا روز تولد را کنید" ,
    birthMonth_cannot_be_empty:"لطفا ماه تولد را وارد کنید" ,
    birthYear_cannot_be_empty:"لطفا سال تولد را وارد کنید" ,
    birthDay_not_valid:"روز تولد نامعتبر است" ,
    birthMonth_not_valid:"ماه تولد نامعتبر است" ,
    birthYear_not_valid:"سال تولد نامعتبر است" ,
    nationalCode_cannot_be_empty:"لطفا کد ملی را وارد کنید" ,
    nationalCode_not_valid:"طول کاراکترهای مجاز 10 تا است" ,
    customerId_not_valid:"شماره مشتری نامعتبر است" ,
    birthYearFrom_cannot_be_bigger_than_birthYearTo:"تاریخ شروع نمیتواند برزگتر از تاریخ پایان باشد",
    companyName_cannnot_be_empty:"لطفا نام شرکت را وارد کنید" ,
    registrationDate_cannot_be_empty:"لطفا تاریخ ثبت را وارد کنید" ,
    registrationDate_not_valid:"تارخ ثبت نامعتبر است" ,
    economicCode_cannot_be_empty:"لطفا کد اقتصادی را وارد کنید" ,



}