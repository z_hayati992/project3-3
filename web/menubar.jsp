<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/8/2020
  Time: 6:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer" ,new Locale("fa"));%>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="view/resource/css/index.css">
</head>
<body>
<div class="navbar" >
    <a href="/index.jsp"><%=resourceBundle.getString("home")%></a>
    <div class="subnav">
        <button class="subnavbtn" ><%=resourceBundle.getString("register")%><i class="fa fa-caret-down"></i></button>
        <div class="subnav-content">
            <a href="${pageContext.request.contextPath}/view/customer/real/registerRealCustomer.jsp"><%=resourceBundle.getString("realCustomer")%></a>
            <a href="${pageContext.request.contextPath}/view/customer/legal/registerLegalCustomer.jsp"><%=resourceBundle.getString("legalCustomer")%></a>
        </div>
    </div>
    <div class="subnav" >
        <button class="subnavbtn"><%=resourceBundle.getString("manage")%><i class="fa fa-caret-down"></i></button>
        <div class="subnav-content">
            <a href="${pageContext.request.contextPath}/view/customer/real/search.jsp"><%=resourceBundle.getString("search")%></a>
        </div>
    </div>
    <a href="#contact"><%=resourceBundle.getString("concat")%></a>
</div>
</body>
</html>