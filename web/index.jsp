<%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/8/2020
  Time: 1:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer" , new Locale("fa"));%>


<html>
<head>
    <title><%=resourceBundle.getString("title")%></title>
    <link rel="stylesheet" type="text/css" href="view/resource/css/index.css">
</head>
<body>
<jsp:include page="menubar.jsp"/>
<h1><%=resourceBundle.getString("message")%></h1>

</body>
</html>
