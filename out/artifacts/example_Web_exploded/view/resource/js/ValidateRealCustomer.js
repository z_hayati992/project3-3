function validate(myForm) {
    var lastName=document.myForm.lastName.value;
    var firstName=document.myForm.firstName.value;
    var fatherName=document.myForm.fatherName.value;
    var birthDay=document.myForm.birthDay.value;
    var birthMonth=document.myForm.birthMonth.value;
    var birthYear=document.myForm.birthYear.value;
    var nationalCode=document.myForm.nationalCode.value;
    if(firstName=="" || firstName==null){
        alert(message.firstName_cannot_be_empty);
        document.myForm.firstName.focus();
        return false;
    }
    if(lastName=="" || lastName==null){
        alert(message.lastName_cannot_be_empty);
        document.myForm.lastName.focus();
        return false;
    }
    if(fatherName=="" || fatherName==null){
        alert(message.fatherName_cannot_be_empty);
        document.myForm.fatherName.focus();
        return false;
    }
    if(birthDay==null || birthDay==""){
        alert(message.birthDay_cannot_be_empty);
        document.myForm.birthDay.focus();
        return false;
    }
    if (birthMonth==null || birthMonth==""){
        alert(message.birthMonth_cannot_be_empty);
        document.myForm.birthMonth.focus();
        return false;
    }
    if(birthYear==null || birthYear==""){
        alert(message.birthYear_cannot_be_empty);
        document.myForm.birthYear.focus();
        return false;
    }
    if(birthDay>31 || birthDay<1){
        alert(message.birthDay_not_valid);
        document.myForm.birthDay.focus();
        return false
    }
    if (birthMonth>12 || birthMonth<1){
        alert(message.birthMonth_not_valid);
        document.myForm.birthMonth.focus();
        return false

    }
    if(birthYear<1000 || birthYear>1400){
        alert(message.birthYear_not_valid);
        document.myForm.birthYear.focus();
        return false;
    }
    if(nationalCode==null || nationalCode==""){
        alert(message.nationalCode_cannot_be_empty)
        document.myForm.nationalCode.focus();
        return false
    }


}

function isNumber() {
    var charCode = event.charCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function validateForm2(myForm) {
    var nationalCode=document.myForm.nationalCode.value;
    var customerId=document.myForm.customerId.value;
    var birthDayFrom=document.myForm.birthDayFrom.value;
    var birthMonthFrom=document.myForm.birthMonthFrom.value;
    var birthYearFrom=document.myForm.birthYearFrom.value;
    var birthDayTo=document.myForm.birthDayTo.value;
    var birthMonthTo=document.myForm.birthMonthTo.value;
    var birthYearTo=document.myForm.birthYearTo.value;
    if(nationalCode!="" && (nationalCode.length<10 || nationalCode.length>10)){
        alert(message.nationalCode_not_valid)
        document.myForm.nationalCode.focus();
        return false
    }
    if(nationalCode<0 || nationalCode>2147483647){
        alert(message.nationalCode_not_valid)
        document.myForm.nationalCode.focus();
        return false

    }
    if(isNaN(customerId)){
        alert(message.customerId_not_valid)
        document.myForm.customerId.focus()
        return false
    }
    if( birthDayFrom!="" && (birthDayFrom<1 ||birthDayFrom>31)){
        alert(message.birthDay_not_valid);
        document.myForm.birthDayFrom.focus();
        return false;
    }
    if (birthMonthFrom!="" &&(birthMonthFrom<1 ||birthMonthFrom>12)){
        alert(message.birthMonth_not_valid);
        document.myForm.birthMonthFrom.focus()
        return false;
    }
    if( birthYearFrom!="" && (birthYearFrom<1000 || birthYearFrom>1400)){
        alert(message.birthYear_not_valid);
        document.myForm.birthYearFrom.focus();
        return false;
    }
    if( birthDayTo!=""  &&(birthDayTo<1 ||birthDayTo>31)){
        alert(message.birthDay_not_valid);
        document.myForm.birthDayTo.focus();
        return false;
    }
    if ( birthMonthTo!="" &&(birthMonthTo<1 ||birthMonthTo>12)){
        alert(message.birthMonth_not_valid);
        document.myForm.birthMonthTo.focus()
        return false;
    }
    if(birthYearTo!="" && (birthYearTo<1000 || birthYearTo>1400)){
        alert(message.birthYear_not_valid);
        document.myForm.birthYearTo.focus();
        return false;
    }
    if(birthYearFrom>birthYearTo) {
        alert(message.birthYearFrom_cannot_be_bigger_than_birthYearTo);
        document.myForm.birthYearFrom.focus();
        return false
    }
    if(birthYearFrom==birthYearTo){
        if(birthMonthFrom>birthMonthTo){
                alert(message.birthYearFrom_cannot_be_bigger_than_birthYearTo);
                document.myForm.birthDayFrom.focus();
                return false
            }
        else if(birthMonthFrom==birthMonthTo){
            if(birthDayFrom>birthDayTo){
                alert(message.birthYearFrom_cannot_be_bigger_than_birthYearTo);
                document.myForm.birthDayFrom.focus();
                return false;
            }
        }
        }



}
