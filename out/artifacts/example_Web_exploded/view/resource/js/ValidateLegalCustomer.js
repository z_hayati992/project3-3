function validate(myForm) {
    var firstName=document.myForm.firstName.value;
    var birthDay=document.myForm.birthDay.value;
    var birthMonth=document.myForm.birthMonth.value;
    var birthYear=document.myForm.birthYear.value;
    var nationalCode=document.myForm.nationalCode.value;
    if(firstName=="" || firstName==null){
        alert(message.companyName_cannnot_be_empty);
        document.myForm.firstName.focus();
        return false;
    }
    if(birthDay==null || birthDay==""){
        alert(message.registrationDate_cannot_be_empty);
        document.myForm.birthDay.focus();
        return false;
    }
    if (birthMonth==null || birthMonth==""){
        alert(message.registrationDate_cannot_be_empty);
        document.myForm.birthMonth.focus();
        return false;
    }
    if(birthYear==null || birthYear==""){
        alert(message.registrationDate_cannot_be_empty);
        document.myForm.birthYear.focus();
        return false;
    }
    if(birthDay>31 || birthDay<1){
        alert(message.registrationDate_not_valid);
        document.myForm.birthDay.focus();
        return false
    }
    if (birthMonth>12 || birthMonth<1){
        alert(message.registrationDate_not_valid);
        document.myForm.birthMonth.focus();
        return false

    }
    if(birthYear<1000 || birthYear>1400){
        alert(message.registrationDate_not_valid);
        document.myForm.birthYear.focus();
        return false;
    }
    if(nationalCode==null || nationalCode==""){
        alert(message.economicCode_cannot_be_empty)
        document.myForm.nationalCode.focus();
        return false
    }


}
function isNumber() {
    var charCode = event.charCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
