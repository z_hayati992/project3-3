<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ResourceBundle" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/13/2020
  Time: 1:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ResourceBundle resourceBundle = ResourceBundle.getBundle("Customer",new Locale("fa"));
%>
<html>
<head>
    <title><%=resourceBundle.getString("result")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
</head>
<body>
<jsp:include page="/menubar.jsp"/>
<%
    if(request.getAttribute("found")==null){
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }
    else if(request.getAttribute("found").equals(true)){%>
<div class="form" style="text-align:center" align="center">
       <p>
           <%=resourceBundle.getString("sorry_There_is_a_customer_with_these_characteristics")%>
         </p>
        <p><a href="${pageContext.request.contextPath}/view/customer/real/registerRealCustomer.jsp"><%=resourceBundle.getString("go_to_back")%></a>
       </p>
</div>
<%} else{%>
<div class="form" style="text-align:center" align="center">
    <p><b><%=resourceBundle.getString("register_customer_successful")%></b></p>
    <br>
    <%=resourceBundle.getString("customerId")%> ${customer.customerId}
    <br><br>
    <p><a href="${pageContext.request.contextPath}/view/customer/real/registerRealCustomer.jsp"><%=resourceBundle.getString("go_to_back")%></a>
    </p>
</div>
<%}%>



</body>
</html>
