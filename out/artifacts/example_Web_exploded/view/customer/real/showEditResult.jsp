<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/14/2020
  Time: 12:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ResourceBundle resourceBundle = ResourceBundle.getBundle("Customer",new Locale("fa"));
%>
<html>
<head>
    <title><%=resourceBundle.getString("edit")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
</head>
<body>
<jsp:include page="/menubar.jsp"/>
<%
    if (request.getParameter("edited") == null)
        return;
    if (request.getParameter("edited").equals("1")) {
%>
<div class="form" align="center">
    <p><%=resourceBundle.getString("edited_success")%></p>
    <p><a href="search.jsp" style="font-size: 16px"><%=resourceBundle.getString("go_to_back")%></a></p>

</div>
<%
} else if (request.getParameter("edited").equals("0")) {
%>
<div class="form" align="center">
    <p><%=resourceBundle.getString("edited_fail")%></p>

    <p><a href="search.jsp" style="font-size: 16px"><%=resourceBundle.getString("go_to_back")%></a></p>
</div>

<%}%>


</body>
</html>
