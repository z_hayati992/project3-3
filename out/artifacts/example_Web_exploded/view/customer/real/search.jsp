﻿<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% ResourceBundle resourceBundle = ResourceBundle.getBundle(
        "Customer" ,new Locale("fa"));%>
<html>
<head>
    <title><%=resourceBundle.getString("search")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/ValidateRealCustomer.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/customer-properties.js"></script>

</head>
<body>
<jsp:include page="/menubar.jsp"/>

<div>
    <form class="form" name="myForm" method="post" action="${pageContext.request.contextPath}/customer/SearchCustomer" onsubmit="return(validateForm2(this));">
    <table style="float: right;" >
        <tr>
            <td>
                <input name="firstName" id="firstName" type="text"  />
            </td>
            <td>
                <label for="firstName"><%=resourceBundle.getString("firstName")%>/<%=resourceBundle.getString("companyName")%></label>

            </td>

        </tr>
        <tr>
            <td>
                <input name="lastName" id="lastName" type="text" />
                </td>
            <td>
                <label for="lastName"><%=resourceBundle.getString("lastName")%></label>

            </td>
        </tr>
        <tr>
            <td>

                <input id="nationalCode" maxlength="10" type="text" name="nationalCode"
                        onkeypress="return(isNumber());"/>
            </td>
            <td>
                <label for="nationalCode"><%=resourceBundle.getString("nationalCode")%>/<%=resourceBundle.getString("economicCode")%></label>

            </td>
        </tr>
        <tr>
            <td>
                <input id="customerId" maxlength="10" type="text" name="customerId"
                        onkeypress="return(isNumber());"/>
            </td>
            <td>
                <label for="customerId"><%=resourceBundle.getString("customerId")%></label>

            </td>
        </tr>

    </table>
    <table style="float: right">
        <tr>
            <td>

                <input id="birthYearFrom" type="text" name="birthYearFrom" maxlength="4" size="3"
                       placeholder="<%=resourceBundle.getString("year")%>"
                               onkeypress="return(isNumber());"/>
                <input id="birthMonthFrom" type="text" name="birthMonthFrom" maxlength="2" size="3"
                       placeholder="<%=resourceBundle.getString("month")%>"
                               onkeypress="return(isNumber());"/>
                <input id="birthDayFrom" type="text" name="birthDayFrom" maxlength="2" size="3"
                       placeholder="<%=resourceBundle.getString("day")%>"
                               onkeypress="return(isNumber());"/>
                <label for="birthDayFrom"><%=resourceBundle.getString("birthDay_from")%></label>
            </td>
        </tr>
            <table style="float: right">
                <tr>
                    <td>

                <input id="birthYearTo" type="text" name="birthYearTo" maxlength="4" size="3"
                       placeholder="<%=resourceBundle.getString("year")%>"
                               onkeypress="return(isNumber());"/>
                <input id="birthMonthTo" type="text" name="birthMonthTo" maxlength="2" size="3"
                       placeholder="<%=resourceBundle.getString("month")%>"
                               onkeypress="return(isNumber())"/>
                <input id="birthDayTo" type="text" name="birthDayTo" maxlength="2" size="3"
                       placeholder="<%=resourceBundle.getString("day")%>"
                               onkeypress="return(isNumber());"/>
                        <label for="birthDayTo"><%=resourceBundle.getString("to")%></label>


            </td>


        </tr>
            </table>
        <table style="float: right" >
            <tr>
                <td>

                    <select name="customerType" id="customerType">
                        <option value="0"><%=resourceBundle.getString("realCustomer")%></option>
                        <option value="1"><%=resourceBundle.getString("legalCustomer")%></option>
                    </select>
                </td>
                <td>
                    <label for="customerType"><%=resourceBundle.getString("customerType")%></label>
                </td>
            </tr>
            <tr >
                <td>
                    <input class="button2" type="submit" value=<%=resourceBundle.getString("search")%>>
                </td>

            </tr>

        </table>
    </table>



</form>
</div>
</body>
</html>
