<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %><%--
  Created by IntelliJ IDEA.
  User: zahra
  Date: 6/8/2020
  Time: 9:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%ResourceBundle resourceBundle = ResourceBundle.getBundle("Customer",new Locale("fa"));%>
<html>
<head>
    <title><%=resourceBundle.getString("register")%></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/resource/css/index.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/ValidateLegalCustomer.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/view/resource/js/customer-properties.js"></script>
</head>
<body>
<jsp:include page="/menubar.jsp"/>
<div align="center">
<div>
    <form class="form" name="myForm" method="post" action="${pageContext.request.contextPath}/register"
          onsubmit="return(validate(this));">
        <p><b><%=resourceBundle.getString("register_legal_customer")%></b></p>
        <table>
            <tr>
                <td>
                    <input id="firstName" maxlength="30" type="text" name="firstName" />
                </td>
                <td>
                    <label for="firstName"><%=resourceBundle.getString("companyName")%></label>
                </td>

            </tr>
            <tr>
                <td>
                    <input id="birthYear" type="text" name="birthYear" maxlength="4" size="3" placeholder="<%=resourceBundle.getString("year")%>"
                            onkeypress="return(isNumber());"/>

                    <input id="birthMonth" type="text" name="birthMonth" maxlength="2" size="2"
                           placeholder="<%=resourceBundle.getString("month")%>" onkeypress="return(isNumber());"/>
                    <input id="birthDay" type="text" name="birthDay" maxlength="2" size="1" placeholder="<%=resourceBundle.getString("day")%>"
                            onkeypress="return(isNumber());"/>

                </td>
                <td>
                    <label for="birthDay"><%=resourceBundle.getString("registrationDate")%></label>
                </td>

            </tr>
            <tr>
                <td>
                    <input id="nationalCode" maxlength="10" type="text" name="nationalCode"
                           onkeypress="return(isNumber());"/>
                </td>
                <td>
                    <label for="nationalCode"><%=resourceBundle.getString("economicCode")%></label>
                </td>

            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <input type="hidden" name="customerType" value="1"/>
                    <input class="button2" type="submit" value=<%=resourceBundle.getString("register")%>>
                </td>
            </tr>
        </table>
    </form>

</div>

</div>

</body>
</html>
